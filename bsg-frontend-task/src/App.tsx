import React, { FunctionComponent } from 'react';
import { connect, ConnectedProps } from 'react-redux';
import styled, { ThemeProvider } from 'styled-components';
import ErrorPopup from './components/ErrorPopup';
import LoginScreen from './LoginScreen';
import MainScreen from './MainScreen';
import SplashScreen from './SplashScreen';
import { RootState } from './store';

const theme = {
  primary: "#f52746",
  secondary: "#0f1c27",
  default: "#000000",
  error: "#f52746"
};

const Content = styled.div`
  position: relative;
  display: flex;
  min-width: 100vw;
  min-height: 100vh;
  max-width: 100vw;
  width: 100vw;
  font-size: 16px;
  font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif, -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif;
`

const App: FunctionComponent<ReduxProps> = ({loading, token}) => {
  const validateToken = () => {
    return token.token !== "" && (new Date(Date.now())).getTime() < token.expires.getTime();
  }
  return (
    <ThemeProvider theme={theme}>
      <Content className="App">
        {loading ? <SplashScreen/> : null}
        {validateToken() ? <MainScreen /> : <LoginScreen />}
        <ErrorPopup />
      </Content>
    </ThemeProvider>
  );
}

const mapStateToProps = (state: RootState) => {
  return {
    loading: state.loading,
    token: state.token
  }
}
const connector = connect(mapStateToProps);
type ReduxProps = ConnectedProps<typeof connector>;
export default connector(App);
