import React, { FunctionComponent, useState } from "react";
import { connect, ConnectedProps } from "react-redux";
import styled from 'styled-components';
import Card from "./components/Card";
import Input from "./components/Input";
import Button from "./components/Button";
import Divider from "./components/Divider";
import Logo from "./components/Logo";
import { RootState } from "./store";
import { logIn } from './actions/Authorization'

const Screen = styled.div`
    flex: 1;
    display: flex;
    align-items: center;
    justify-content: center;
    ${Card} {
        padding: 1vw;
        min-width: 300px;
        width: 20vw;
    }
`

const StyledForm = styled.form`
    padding: 0;
    width: calc(100% - 1vw);
    margin-bottom: 12px;
`

const FullWidthButton = styled(Button)`
    width: calc(100% - 4px);
    margin: 2px;
`

const LoginScreen: FunctionComponent<ReduxProps> = (props: ReduxProps) => {
    const [login, setLogin] = useState("");
    const [password, setPassword] = useState("");

    const tryLogin = () => {
        props.logIn(login, password);
    }

    const enterAsGuest = () => {
        props.logIn("", "");
    }
    return (
        <Screen>
            <Card>
                <Logo width={75} height={75} strokeWidth={2} padding={5}/>
                <StyledForm onSubmit={(event) => {
                    event.preventDefault();
                }}>
                    <Input 
                        id="login" 
                        name="login" 
                        autoComplete="on"
                        label="User name" 
                        onValueChange={(value: string)=> setLogin(value)} 
                    />
                    <Input 
                        id="password" 
                        name="login" 
                        autoComplete="on"
                        label="Password" 
                        type="password" 
                        errorMessage={props.loginError !== "" ? props.loginError : undefined}
                        onValueChange={(value: string)=> setPassword(value)}
                    />
                </StyledForm>

                <FullWidthButton color="primary" onClick={tryLogin}>
                    Sign in
                </FullWidthButton>
                <Divider />
                <FullWidthButton color="primary" inverted disabled>
                    Sign up
                </FullWidthButton>
                <FullWidthButton color="secondary" onClick={enterAsGuest}>
                    Enter as guest
                </FullWidthButton>
            </Card>
        </Screen>
    )
}

const mapStateToProps = (state: RootState) => {
    return {
        loginError: state.loginError
    }
}

const connector = connect(mapStateToProps, {
    logIn,
});
type ReduxProps = ConnectedProps<typeof connector>;
export default connector(LoginScreen);