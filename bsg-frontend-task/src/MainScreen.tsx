import { connect, ConnectedProps } from "react-redux";
import { FunctionComponent, useCallback, useEffect } from "react"
import { fetchFilms } from "./actions/MoviesActions"
import styled from "styled-components";
import MovieList from "./components/MovieList";
import { RootState } from "./store";
import { loading } from "./actions/LoadingAction";
import { Movie } from "./reducers/MoviesReducer";
import MoviePlayer from "./components/Player";

const Screen = styled.div`
    flex-grow: 1;
    flex-shrink: 1;
    display: flex;
    align-items: flex-start;
    flex-direction: column;
    width: 97vw;
    padding: 1vw;
`

const preloadImages = async (movieLists: Array<Array<Movie>>) => {
    const imagesLoadPromises = [];
    for(const list of movieLists){
        for(const movie of list){
            const frameImage = movie.Images.find(image => image.ImageTypeCode === "FRAME");
            if(frameImage){
                imagesLoadPromises.push(new Promise(function (resolve, reject) {
                    const img = new Image();
                    img.src = frameImage.Url;
                    img.onload = resolve;
                    img.onerror = reject;
                }))
            }
        }
    }
    await Promise.all(imagesLoadPromises);
}

const MainScreen: FunctionComponent<ReduxProps> = ({ fetchFilms, movieLists, loading, playingMovieId }) => {
    const fetchFilmsCallbacks = useCallback(fetchFilms, [fetchFilms])
    useEffect(()=>{
        fetchFilmsCallbacks();
    }, [fetchFilmsCallbacks]);
    const loadingCallback = useCallback(loading, [loading])
    useEffect(()=>{
        (async () => {
            if(movieLists.length > 0) {
                loadingCallback(true);
                await preloadImages(movieLists);
                loadingCallback(false);
            }
        })();
    }, [movieLists, loadingCallback]);

    return (
        <Screen>
            {
                movieLists.map((list, index, array) => {
                    return <MovieList key={`list-${index}`} movies={list} title={`Lista ${index}`}/>
                })
            }
            { playingMovieId >= 0 ? <MoviePlayer /> : null }
        </Screen>
    )
}

const mapStateToProps = (state: RootState) => {
    return {
        movieLists: state.movieLists,
        playingMovieId: state.playingMovieId,
    }
}

const connector = connect(mapStateToProps, {
    fetchFilms,
    loading
});
type ReduxProps = ConnectedProps<typeof connector>;
export default connector(MainScreen);