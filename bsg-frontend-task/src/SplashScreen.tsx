import React, { FunctionComponent } from "react";
import styled from 'styled-components';
import Loader from "./components/Loader";
 
const Screen = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 3;
    width: 100vw;
    height: 100vh;
    background-color: white;

    display: flex;
    align-items: center;
    justify-content: center;
`

const SplashScreen: FunctionComponent = () => {
    return (
        <Screen>
            <Loader width={window.innerWidth/8} height={window.innerWidth/8} strokeWidth={5} padding={10}/>
        </Screen>
    )
}

export default SplashScreen;