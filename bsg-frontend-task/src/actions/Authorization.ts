import getDeviceData from "../helpers/deviceDataHelper";
import { RootState } from "../store";
import { ThunkAction } from "redux-thunk"
import { loading, LoadingAction } from './LoadingAction';
import myAxios from './axios-config';

export type UpdateTokenAction = {
    type: "UPDATE_TOKEN",
    payload: {
        token: string,
        expires: Date
    }
}
export type LoginErrorAction = {
    type: "LOGIN_ERROR",
    payload: string
}
export type SetTokenRefreshLoopAction = {
    type: "SET_TOKEN_REFRESH_LOOP",
    payload: NodeJS.Timeout
}
export type UserTypeAction = {
    type: "USER_TYPE",
    payload: "GUEST" | "NORMAL"
}
export type AuthorizationActions = UpdateTokenAction | LoginErrorAction | SetTokenRefreshLoopAction | LoadingAction | UserTypeAction;
type ThunkResult<R> = ThunkAction<R, RootState, undefined, AuthorizationActions>

export const logIn = (login: string, password: string, background: boolean = false) : ThunkResult<Promise<void>> => {
    return async (dispatch, getState) => {
        const deviceData = getDeviceData();
        if(!background){
            dispatch(loading(true));
        }
        myAxios().post("Authorization/SignIn", {
            "Username": login,
            "Password": password,
            "Device": deviceData
        }).then((value: any) => {
            if(!background){
                dispatch(loading(false));
            }
            const token = value.data.AuthorizationToken.Token;
            const expires = new Date(value.data.AuthorizationToken.TokenExpires as string);
            dispatch(updateToken(token, expires));
            dispatch(loginError(""));
            
            if(login === ""){
                dispatch(userType("GUEST"))
            }
            else {
                dispatch(userType("NORMAL"))
            }

            const { tokenRefreshLoop } = getState();
            if(tokenRefreshLoop.ref){
                clearTimeout(tokenRefreshLoop.ref);
            }
            dispatch({
                type: "SET_TOKEN_REFRESH_LOOP",
                payload: setTimeout(()=>{
                    logIn(login, password, true)(dispatch, getState, undefined)
                }, expires.getTime() - (new Date(Date.now())).getTime() - 2000)
            })
        }).catch((reason: any) => {
            if(!background){
                dispatch(loading(false));
            }
            dispatch(loginError(reason.data.Message))
        })
    }
}

export function loginError(error: string): LoginErrorAction{
    return {
        type: "LOGIN_ERROR",
        payload: error
    }
}

export function updateToken(token: string, expires: Date): UpdateTokenAction{
    return {
        type: "UPDATE_TOKEN",
        payload: {
            token,
            expires
        }
    }
}

export function userType(type: "GUEST" | "NORMAL"): UserTypeAction{
    return {
        type: "USER_TYPE",
        payload: type
    }
}

