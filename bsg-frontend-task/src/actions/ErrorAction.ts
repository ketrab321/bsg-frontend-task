export type ErrorAction = {
    type: "GLOBAL_ERROR",
    payload: string
}

export function setErrorMessage(error: any): ErrorAction {
    if(typeof error === "string"){
        return {
            type: "GLOBAL_ERROR",
            payload: error
        }
    }
    else if(error && error.hasOwnProperty("response") && error.response.hasOwnProperty("data")){
        return {
            type: "GLOBAL_ERROR",
            payload: error.response.data.Message
        }
    }
    else{
        return {
            type: "GLOBAL_ERROR",
            payload: JSON.stringify(error)
        }
    }
}