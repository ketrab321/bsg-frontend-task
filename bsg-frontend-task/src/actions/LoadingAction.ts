export function loading(isLoading: boolean): LoadingAction {
    return {
        type: "LOADING",
        payload: isLoading
    }
}

export type LoadingAction = {
    type: "LOADING",
    payload: boolean
}