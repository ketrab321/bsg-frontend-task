import { RootState } from "../store";
import { ThunkAction } from "redux-thunk"
import myAxios from './axios-config';
import { Movie } from "../reducers/MoviesReducer";
import { loading, LoadingAction } from "./LoadingAction";

export type FetchMoviesAction = {
    type: "FETCH_MOVIE_LISTS",
    payload: Array<Array<Movie>>
}


type Response = {
    "Entities": Array<Movie>,
    "PageSize": number,
    "PageNumber": number,
    "TotalCount": number,
}


export type MoviesActions = FetchMoviesAction | LoadingAction;
type ThunkResult<R> = ThunkAction<R, RootState, undefined, MoviesActions>

export const fetchFilms = () : ThunkResult<Promise<void>> => {
    return async (dispatch, getState) => {
        dispatch(loading(true));
        const movieLists: Array<Array<Movie>> = [];
        const { token } = getState();
        const promises = [];
        for(let i = 1; i < 20; i++){
            promises.push(myAxios(token).post<Response>('Media/GetMediaList', {
                "MediaListId": i,
                "IncludeCategories": false,
                "IncludeImages": true,
                "IncludeMedia": false,
                "PageNumber": 1,
                "PageSize": 15
            }))
        }
        Promise.all(promises).then((responses) => {
            for(const response of responses){
                if(response && response.data){
                    movieLists.push(response.data.Entities)
                }
            }
            dispatch({
                type: "FETCH_MOVIE_LISTS",
                payload: movieLists
            })
            dispatch(loading(false));

        }).catch((reason: any) => {
            dispatch(loading(false));
        })
    }
}
