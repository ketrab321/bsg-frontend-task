import myAxios from "./axios-config"
import { RootState } from "../store";
import { ThunkAction } from "redux-thunk"
import { MoviePlayingInfo } from "../reducers/PlayerReducers";

type ThunkResult<R> = ThunkAction<R, RootState, undefined, MovieInfoAction>

export type PlayingMovieIdAction = {
    type: "PLAYING_MOVIE_ID",
    payload: number
}
export type MovieInfoAction = {
    type: "PLAYING_MOVIE_INFO",
    payload: MoviePlayingInfo
}

export function fetchMovieInfo(): ThunkResult<Promise<void>> {
    return async (dispatch, getState) => {
        const { token, playingMovieId, userType } = getState();
        myAxios(token).post<MoviePlayingInfo>("Media/GetMediaPlayInfo", {
            "MediaId": playingMovieId,
            "StreamType": userType === "GUEST" ? "TRIAL" : "MAIN"
        }).then((response) => {
            dispatch({
                type: "PLAYING_MOVIE_INFO",
                payload: response.data
            })
        })
    }
}

export function setPlayingMovieId(id: number | undefined): PlayingMovieIdAction {
    return {
        type: "PLAYING_MOVIE_ID",
        payload: id ? id : -1
    }
}
