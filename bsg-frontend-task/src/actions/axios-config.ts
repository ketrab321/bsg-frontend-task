import axios from "axios";
import { Token } from "../reducers/AuthorizationReducers";
import store from '../store';
import { setErrorMessage } from "./ErrorAction";

const myAxios = (token: Token | undefined = undefined) => {
    const axiosClient = axios.create({
        baseURL: "https://thebetter.bsgroup.eu/",
        headers: {
            'Content-Type': 'application/json',
            'Authorization': token ? `Bearer ${token.token}` : null
        },
        responseType: 'json',
    })

    axiosClient.interceptors.response.use(function (response) {
        return response;
    }, function (error) {
        if (typeof error.response === 'undefined') {
            alert('A network error occurred. '
                + 'This could be a CORS issue or a dropped internet connection. '
                + 'It is not possible for us to know.')
            error.data = {
                Message: "CORS Error"
            }
            throw error;
        }
        else{
            store.dispatch(setErrorMessage(error))
            throw error.response;
        }
    });

    return axiosClient;
};

export default myAxios;


