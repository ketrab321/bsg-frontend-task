import React, { FunctionComponent, ButtonHTMLAttributes } from "react";

import styled from "styled-components";

const StyledButton = styled.button`
    border-radius: 32px;
    padding: 12px;
    font-weight: bold;

    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;

    :hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
        transform: scale(1.05);
    }
`

const StyledButtonNormal = styled(StyledButton)`
    color: white;
    background-color: ${(props) => props.theme[props.color!]};
    border-color: white;
`

const StyledButtonInverted = styled(StyledButton)`
    color: ${(props) => props.theme[props.color!]};
    background-color: white;
    border-color: ${(props) => props.theme[props.color!]};
`

type Props = {
    color: "primary"|"secondary"|"default",
    inverted?: boolean
} & ButtonHTMLAttributes<HTMLButtonElement>

const Button: FunctionComponent<Props> = (props: Props) => {
    const {color, inverted, children, ...otherProps} = props;
    return (
        <React.Fragment>
            {
                inverted ? (
                    <StyledButtonInverted color={color} {...otherProps}>
                        {children}
                    </StyledButtonInverted>
                ) : (
                    <StyledButtonNormal color={color} {...otherProps}>
                        {children}
                    </StyledButtonNormal>
                )
            }
        </React.Fragment>
    )
}

export default Button;