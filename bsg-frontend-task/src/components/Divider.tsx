import styled from "styled-components";

const Divider = styled.hr`
    width: 100%;
`

export default Divider;