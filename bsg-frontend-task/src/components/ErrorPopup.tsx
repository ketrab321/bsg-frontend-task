import React, { FunctionComponent } from 'react'
import { RootState } from "../store";
import { connect, ConnectedProps } from "react-redux";
import styled from 'styled-components';
import { setErrorMessage } from '../actions/ErrorAction';

const ErrorContainer = styled.div`
    z-index: 10;
    position: fixed;
    width: 100vw;
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: rgba(0,0,0,0.4);

    .player {
        z-index: 10;
    }
`

const Text = styled.div`
    bottom: 0;
    color: white;
    font-size: 30px;
    font-weight: bold;
    text-shadow: -1px 1px 5px ${(props) => props.theme.error},
                  1px 1px 5px ${(props) => props.theme.error},
                  1px -1px 5px ${(props) => props.theme.error},
                 -1px -1px 5px ${(props) => props.theme.error};
`

const ErrorPopup: FunctionComponent<ReduxProps> = ({error, setErrorMessage}) => {
    if(error !== "") {
        return (
            <ErrorContainer onClick={()=>{setErrorMessage("")}}>
                <Text>{error}</Text>
            </ErrorContainer>
        )
    }
    
    return null;
}


const mapStateToProps = (state: RootState) => {
    return {
        error: state.error
    }
}

const connector = connect(mapStateToProps, {
    setErrorMessage
});
type ReduxProps = ConnectedProps<typeof connector>;
export default connector(ErrorPopup);