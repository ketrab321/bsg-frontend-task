import React, { FunctionComponent, useState, InputHTMLAttributes } from "react";

import styled from "styled-components";

const StyledDiv = styled.div`
    display: flex;
    flex-direction: column;
    position: relative;
    margin: 1vh;
`

const StyledLabel = styled.label`
    background-color: white;
    position: absolute;
    padding: 2px;
    left: 6px;
`

const StyledInput = styled.input`
    padding: 8px;
    margin-top: 14px;
`

const StyledParagraph = styled.p`
    color: ${(props) => props.theme.error};
    padding: 0;
    margin: 0;
    text-align: right;
`

type Props = InputHTMLAttributes<HTMLInputElement> & {
    id: string,
    label: string,
    onValueChange: (value: string) => void,
    errorMessage?: string,
    value?: string
} 

const Input: FunctionComponent<Props> = (props: Props) => {
    const {id, label, onValueChange, errorMessage, value, ...otherProps} = props
    const [inputValue, changeInputValue] = useState<string>(value || "");

    const onChange = (event: any) => {
        onValueChange(event.target.value);
        changeInputValue(event.target.value);
    }

    return (
        <StyledDiv>
            <StyledLabel htmlFor={id}>{label}</StyledLabel>
            <StyledInput {...otherProps} value={inputValue} onChange={onChange}/>
            {
                props.errorMessage ? <StyledParagraph>{errorMessage}</StyledParagraph> : null
            }
        </StyledDiv>
    )
}

export default Input;