import React from "react";
import styled, { keyframes } from 'styled-components';

const draw = (lineLength: number, strokeWidth: number) => keyframes`
    from {
        stroke-width: ${strokeWidth/10};
        stroke-dashoffset: ${lineLength};
        stroke-opacity: 0;
    }
    to {
        stroke-width: ${strokeWidth};
        stroke-dashoffset: 0;
        stroke-opacity: 1;
    }
`
const LoaderWrapper = styled.svg<{lineLength: number, strokeWidth: number}>`
    .loader {
        fill: none;
        stroke: #f52747;
        stroke-width: ${props => props.strokeWidth};
        stroke-linecap: round;
        stroke-linejoin: round;
        stroke-miterlimit: 4;
        stroke-dasharray: ${props => props.lineLength};
        stroke-dashoffset: ${props => props.lineLength};
        stroke-opacity: 1;
        animation: ${props => draw(props.lineLength, props.strokeWidth)} 2000ms linear 0ms infinite alternate-reverse;
    }
`

type Props = {
    height: number,
    width: number,
    strokeWidth: number,
    padding: number,
}
function Loader(props: Props) {
    const startWidth = 660;
    const startHeight = 512;
    const widthRatio = props.width/startWidth;
    const heightRatio = props.height/startHeight;

    const lineLength = 1000*widthRatio + 1000*heightRatio;

    return (
        <LoaderWrapper viewBox={`${-props.padding} ${-props.padding} ${props.width + 2*props.padding} ${props.height + 2*props.padding}`}
            width={`${props.width}`} 
            height={`${props.height}`}
            lineLength={lineLength}
            strokeWidth={props.strokeWidth}
        >
            <path
                d={`M   ${7.3959652*widthRatio},${296.69624*heightRatio} 
                        ${147.43845*widthRatio},${34.436063*heightRatio} 
                        ${464.13976*widthRatio},${215.30514*heightRatio} 
                        ${305.82997*widthRatio},${305.73968*heightRatio} 
                        V ${7.3056946*heightRatio} 
                        H ${649.48126*widthRatio} 
                        L ${464.56552*widthRatio},${97.740234*heightRatio} 
                        ${464.13976*widthRatio},${215.30514*heightRatio}`}
                id="loader1"
                className="loader"
            />
            <path
                d={`M   ${645.38565*widthRatio},${504.69569*heightRatio} 
                        ${147.97107*widthRatio},${215.30514*heightRatio} 
                        ${305.83014*widthRatio},${124.87059*heightRatio} 
                        ${464.13976*widthRatio},${215.30514*heightRatio} Z`}
                id="loader2"
                className="loader"
            />
            <path
                d={`M   ${464.56552*widthRatio},${97.740234*heightRatio} 
                        ${305.82997*widthRatio},${7.3056946*heightRatio} 
                        ${305.83014*widthRatio},${124.87059*heightRatio} 
                        ${7.3959652*widthRatio},${296.69624*heightRatio}`}
                id="loader3"
                className="loader"
            />
        </LoaderWrapper> 
    )
}

export default Loader;