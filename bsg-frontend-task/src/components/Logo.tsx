import React from "react";
import styled, { keyframes } from 'styled-components';

const draw = (lineLength: number) => keyframes`
    from {
        stroke-dashoffset: ${lineLength};
    }
    to {
        stroke-dashoffset: 0;
    }
`

const LogoWrapper = styled.svg<{strokeWidth: number, lineLength: number}>`
    .logo {
        fill: none;
        stroke: #f52747;
        stroke-width: ${props => props.strokeWidth};
        stroke-linecap: round;
        stroke-linejoin: round;
        stroke-miterlimit: 4;
        stroke-opacity: 1;
        stroke-dasharray: ${props => props.lineLength};
        stroke-dashoffset: ${props => props.lineLength};
        animation: ${props => draw(props.lineLength)} 3000ms linear 0ms infinite alternate-reverse;
    }
`

type Props = {
    height: number,
    width: number,
    strokeWidth: number,
    padding: number,
}
function Logo(props: Props) {
    const startWidth = 660;
    const startHeight = 512;
    const widthRatio = props.width/startWidth;
    const heightRatio = props.height/startHeight;

    const lineLength = 1000*widthRatio + 1000*heightRatio;

    return (
        <LogoWrapper viewBox={`${-props.padding} ${-props.padding} ${props.width + 2*props.padding} ${props.height + 2*props.padding}`}
            width={`${props.width}`} 
            height={`${props.height}`}
            lineLength={lineLength}
            strokeWidth={props.strokeWidth}
        >
            <path
                d={`M   ${7.3959652*widthRatio},${296.69624*heightRatio} 
                        ${147.43845*widthRatio},${34.436063*heightRatio} 
                        ${464.13976*widthRatio},${215.30514*heightRatio} 
                        ${305.82997*widthRatio},${305.73968*heightRatio} 
                        V ${7.3056946*heightRatio} 
                        H ${649.48126*widthRatio} 
                        L ${464.56552*widthRatio},${97.740234*heightRatio} 
                        ${464.13976*widthRatio},${215.30514*heightRatio}`}
                id="logo1"
                className="logo"
            />
            <path
                d={`M   ${645.38565*widthRatio},${504.69569*heightRatio} 
                        ${147.97107*widthRatio},${215.30514*heightRatio} 
                        ${305.83014*widthRatio},${124.87059*heightRatio} 
                        ${464.13976*widthRatio},${215.30514*heightRatio} Z`}
                id="logo2"
                className="logo"
            />
            <path
                d={`M   ${464.56552*widthRatio},${97.740234*heightRatio} 
                        ${305.82997*widthRatio},${7.3056946*heightRatio} 
                        ${305.83014*widthRatio},${124.87059*heightRatio} 
                        ${7.3959652*widthRatio},${296.69624*heightRatio}`}
                id="logo3"
                className="logo"
            />
        </LogoWrapper> 
    )
}

export default Logo;