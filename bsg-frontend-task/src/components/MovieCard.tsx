import { FunctionComponent } from "react"
import styled from "styled-components"
import Card from "./Card"
import { Movie } from "../reducers/MoviesReducer"
import { connect, ConnectedProps } from "react-redux"
import { setPlayingMovieId } from "../actions/PlayerActions";

const StyledCard = styled(Card)`
    position: relative;
    width: 200px;
    height: 112px;
    margin: 10px;

    :hover {
        transform: scale(1.05);
    }
`

const Image = styled.img`
    width: 100%;
    height: 100%;
    border-radius: 10px;
`

const Title = styled.div`
    position: absolute;
    padding: 5px;
    bottom: 0;
    color: white;
    font-size: 20px;
    font-weight: bold;
    text-shadow: -1px 1px 5px #000000, 1px 1px 5px #000000, 1px -1px 5px #000000; -1px -1px 5px #000000;
`

type Props = {
    movie: Movie
} & ReduxProps

const MovieCard: FunctionComponent<Props> = ({movie, setPlayingMovieId}) => {
    const frameImage = movie.Images.find(image => image.ImageTypeCode === "FRAME");
    return (
        <li onClick={() => {setPlayingMovieId(movie.Id)}}>
            <StyledCard>
                <Title>{movie.Title}</Title>
                <Image src={frameImage ? frameImage.Url : ""} alt="Cover image"/>
            </StyledCard>
        </li>
    )
}

const connector = connect(undefined, {
    setPlayingMovieId
});
type ReduxProps = ConnectedProps<typeof connector>;
export default connector(MovieCard);
