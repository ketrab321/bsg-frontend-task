import React, { FunctionComponent } from "react"
import styled from "styled-components"
import MovieCard from "./MovieCard"
import { Movie } from "../reducers/MoviesReducer"

const List = styled.ul`
    display: flex;
    overflow-x: auto;
    margin: 0;
    padding-left: 10px;
    padding-right: 10px;
    width: 100%;
    align-items: flex-start;
    box-sizing: border-box;
    margin-block-start: 0;
    margin-block-end: 0;
    margin-inline-start: 0;
    margin-inline-end: 0;
    padding-inline-start: 0;
`

const Title = styled.div`
    bottom: 0;
    color: white;
    font-size: 30px;
    font-weight: bold;
    text-shadow: -1px 1px 5px ${(props) => props.theme.secondary},
                  1px 1px 5px ${(props) => props.theme.secondary},
                  1px -1px 5px ${(props) => props.theme.secondary},
                 -1px -1px 5px ${(props) => props.theme.secondary};
`
type Props = {
    movies: Array<Movie>,
    title: string
}

const MovieList: FunctionComponent<Props> = ({movies, title}) => {
    return (
        <React.Fragment>
            <Title>{title}</Title>
            <List>
            {
                movies.map((movie, index, array) => {
                    return <MovieCard key={`movie-${index}`} movie={movie}/>
                })
            }
        </List>
        </React.Fragment>
        
    )
}

export default MovieList;