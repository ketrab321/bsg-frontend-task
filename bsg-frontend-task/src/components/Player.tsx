import React, { FunctionComponent, useEffect } from 'react'
import { RootState } from "../store";
import ReactPlayer from 'react-player'
import { connect, ConnectedProps } from "react-redux";
import { fetchMovieInfo, setPlayingMovieId } from "../actions/PlayerActions";
import { isEmpty } from "../helpers/isObjectEmptyHelper"
import { MoviePlayingInfo } from '../reducers/PlayerReducers';
import styled from 'styled-components';

const PlayerContainer = styled.div`
    z-index: 10;
    position: fixed;
    width: 100vw;
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: rgba(0,0,0,0.4);

    .player {
        z-index: 10;
    }
`

const Text = styled.div`
    bottom: 0;
    color: white;
    font-size: 30px;
    font-weight: bold;
    text-shadow: -1px 1px 5px ${(props) => props.theme.secondary},
                  1px 1px 5px ${(props) => props.theme.secondary},
                  1px -1px 5px ${(props) => props.theme.secondary},
                 -1px -1px 5px ${(props) => props.theme.secondary};
`

const MoviePlayer: FunctionComponent<ReduxProps> = ({playingMovieId, playingMovieInfo, fetchMovieInfo, setPlayingMovieId}) => {
    useEffect(()=>{
        if(playingMovieId >= 0){
            fetchMovieInfo();
        }
    }, [playingMovieId, fetchMovieInfo])

    if(!isEmpty(playingMovieInfo)){
        const info = (playingMovieInfo as MoviePlayingInfo);
        if(info.MediaId === playingMovieId) {
            return (
                <PlayerContainer onClick={()=>{setPlayingMovieId(-1)}}>
                    { info.ContentUrl ? <ReactPlayer url={info.ContentUrl} controls/> : <Text>No content url found!</Text>}
                </PlayerContainer>
            )
        }
    }
    
    return null;
}


const mapStateToProps = (state: RootState) => {
    return {
        playingMovieId: state.playingMovieId,
        playingMovieInfo: state.playingMovieInfo
    }
}

const connector = connect(mapStateToProps, {
    fetchMovieInfo,
    setPlayingMovieId
});
type ReduxProps = ConnectedProps<typeof connector>;
export default connector(MoviePlayer);