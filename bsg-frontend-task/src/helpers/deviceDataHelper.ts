export type DeviceData = {
    "Name": string,
    "PlatformCode": string,
    "FirebaseToken": null,
    "DpiCode": null
    
}

export default function getDeviceData() : DeviceData {
    type tableType = Array<{
        name: string,
        value: string,
        version: string
    }>
    const os: tableType = [
        { name: 'Windows Phone', value: 'Windows Phone', version: 'OS' },
        { name: 'Windows', value: 'Win', version: 'NT' },
        { name: 'iPhone', value: 'iPhone', version: 'OS' },
        { name: 'iPad', value: 'iPad', version: 'OS' },
        { name: 'Kindle', value: 'Silk', version: 'Silk' },
        { name: 'Android', value: 'Android', version: 'Android' },
        { name: 'PlayBook', value: 'PlayBook', version: 'OS' },
        { name: 'BlackBerry', value: 'BlackBerry', version: '/' },
        { name: 'Macintosh', value: 'Mac', version: 'OS X' },
        { name: 'Linux', value: 'Linux', version: 'rv' },
        { name: 'Palm', value: 'Palm', version: 'PalmOS' }
    ];

    const header = [
        navigator.platform,
        navigator.userAgent,
        navigator.appVersion,
        navigator.vendor
    ];


    function matchItem(string: string, data: tableType) {
        var i = 0,
            j = 0,
            regex,
            regexv,
            match,
            matches,
            matchesString,
            version;
        
        for (i = 0; i < data.length; i += 1) {
            regex = new RegExp(data[i].value, 'i');
            match = regex.test(string);
            if (match) {
                regexv = new RegExp(data[i].version + '[- /:;]([._]+)', 'i');
                matches = string.match(regexv);
                version = '';
                if (matches) { if (matches[1]) { matchesString = matches[1]; } }
                if (matchesString) {
                    matchesString = matchesString.split(/[._]+/);
                    for (j = 0; j < matchesString.length; j += 1) {
                        if (j === 0) {
                            version += matchesString[j] + '.';
                        } else {
                            version += matchesString[j];
                        }
                    }
                } else {
                    version = '0';
                }
                return {
                    name: data[i].name,
                    version: parseFloat(version)
                };
            }
        }
        return { name: 'unknown', version: 0 };
    }
    var agent = header.join(' ');
    var osData = matchItem(agent, os);

    return {
        "Name": osData.name,
        "PlatformCode": osData.version + "",
        "FirebaseToken": null,
        "DpiCode": null
    }
}