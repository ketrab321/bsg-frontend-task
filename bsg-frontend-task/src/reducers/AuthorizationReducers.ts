import { AuthorizationActions, UserTypeAction } from "../actions/Authorization";

export type Token = {
    token: string,
    expires: Date
}

export function tokenReducer(token: Token = { token: "", expires: new Date(0)}, action: AuthorizationActions): Token {
    if(action.type === "UPDATE_TOKEN"){
        return action.payload;
    }
    return token;
}

export function tokenRefreshLoopReducer(loop: { ref: NodeJS.Timeout | null } = { ref: null }, action: AuthorizationActions) {
    if(action.type === "SET_TOKEN_REFRESH_LOOP"){
        return {
            ref: action.payload
        }
    }
    return loop;
}

export function loginErrorReducer(error: string = "", action: AuthorizationActions) {
    if(action.type === "LOGIN_ERROR"){
        return action.payload;
    }
    return error;
}

export function userTypeReducer(type: "GUEST" | "NORMAL" = "GUEST", action: UserTypeAction) {
    if(action.type === "USER_TYPE"){
        return action.payload;
    }
    return type;
}