import { ErrorAction } from "../actions/ErrorAction";

export function errorReducer(error: string = "", action: ErrorAction) {
    if(action.type === "GLOBAL_ERROR") {
        return action.payload;
    }
    return error;
}