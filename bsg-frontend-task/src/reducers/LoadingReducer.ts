import { LoadingAction } from "../actions/LoadingAction";

export function loadingReducer(loading = false, action: LoadingAction){
    if(action.type === "LOADING"){
        return action.payload;
    }
    return loading;
}