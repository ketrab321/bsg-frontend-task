import { FetchMoviesAction } from "../actions/MoviesActions"

export type Image = {
    "Id": number,
    "MediaId": number,
    "PlatformCode": string,
    "ImageTypeCode": string,
    "Url": string,
    "Width": number,
    "Height": number
}

export type Movie = {
    "Id": number,
    "Guid": string,
    "MediaTypeCode": string,
    "MediaTypeDisplayName": string,
    "MediaAgeRestrictionValueMin": number,
    "MediaAgeRestrictionImageUrl": string,
    "Title": string,
    "Description": string,
    "Year": number,
    "Duration": number,
    "IsTrialContentAvailable": boolean,
    "AvailableFrom": string,
    "Images": Array<Image>
}

export function movieListsReducer(lists: Array<Array<Movie>> = [], action: FetchMoviesAction) {
    if(action.type === "FETCH_MOVIE_LISTS"){
        return action.payload;
    }
    return lists;
}