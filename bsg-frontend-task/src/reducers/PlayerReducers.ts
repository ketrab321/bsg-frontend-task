import { MovieInfoAction, PlayingMovieIdAction } from "../actions/PlayerActions";

export function playingMovieIdReducer(id: number = -1, action: PlayingMovieIdAction) {
    if(action.type === "PLAYING_MOVIE_ID"){
        return action.payload;
    }
    return id;
}

export type MoviePlayingInfo = {
    "MediaId": number,
    "Title": string,
    "Description": string,
    "MediaTypeCode": string,
    "MediaTypeDisplayName": string,
    "StreamId": number,
    "Provider": string,
    "ContentUrl": string
}

export function playingMovieInfoReducer(info: MoviePlayingInfo | {} = {}, action: MovieInfoAction): MoviePlayingInfo | {} {
    if(action.type === "PLAYING_MOVIE_INFO"){
        return action.payload;
    }
    return info;
}