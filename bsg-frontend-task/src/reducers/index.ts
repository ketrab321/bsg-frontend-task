import { combineReducers } from 'redux';
import { loadingReducer } from './LoadingReducer';
import { tokenReducer, userTypeReducer, tokenRefreshLoopReducer, loginErrorReducer } from './AuthorizationReducers';
import { movieListsReducer } from './MoviesReducer';
import { playingMovieIdReducer, playingMovieInfoReducer } from './PlayerReducers';
import { errorReducer } from './ErrorReducer';

const reducers = combineReducers({
    loading: loadingReducer,
    token: tokenReducer,
    tokenRefreshLoop: tokenRefreshLoopReducer,
    userType: userTypeReducer,
    loginError: loginErrorReducer,
    movieLists: movieListsReducer,
    playingMovieId: playingMovieIdReducer,
    playingMovieInfo: playingMovieInfoReducer,
    error: errorReducer
})

export default reducers;